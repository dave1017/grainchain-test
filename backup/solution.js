const fs = require('fs');
const _ = require('lodash');
let matrix = [
	'00010111',
	'01000000',
	'01000111',
	'01111000',
	'01001000',
	'01001010',
	'01011010',
	'00001000',
	'01101101',
	'00001000',
	'10010011',
	'00100000'
];

let processedMatrix = [];
let itemNumber = 0;
console.log('Matrix ', matrix.length)
for(let i=0;i<matrix.length;i++){
	// console.log('Cadena: ', matrix[i])
	for(let j=0;j<matrix[i].length;j++){
		processedMatrix.push({
			itemNumber: itemNumber,
			row: i,
			column: j,
			value: matrix[i][j],
			isIlluminated: false
		});
		itemNumber++;
		// console.log('valor: ', matrix[i][j])
	}
}

// fs.writeFileSync('input.json', JSON.stringify(processedMatrix, null, 4), 'utf8')

function findDarkArea(darkAreas){
	processedMatrix.forEach(function(pos) {
		if(!pos.isIlluminated && pos.value == 0){
			// console.log('Process position')
			// console.log(JSON.stringify(pos));
			let items = [];
			let item, up, right, down, left;
			item = _.find(processedMatrix, function(element) {
				return element.row == (pos.row-1) && element.column == pos.column && element.value == 0 && !element.isIlluminated;
			});
			if(item) {
				items.push(item);
				up = true;
			}
			item = _.find(processedMatrix, function(element) {
				return element.row == pos.row && element.column == (pos.column+1) && element.value == 0 && !element.isIlluminated;
			});
			if(item) {
				items.push(item);
				right = true;
			}
			item = _.find(processedMatrix, function(element) {
				return element.row == (pos.row+1) && element.column == pos.column && element.value == 0 && !element.isIlluminated;
			});
			if(item) {
				items.push(item);
				down = true;
			}
			item = _.find(processedMatrix, function(element) {
				return element.row == pos.row && element.column == (pos.column-1) && element.value == 0 && !element.isIlluminated;
			});
			if(item) {
				items.push(item);
				left = true;
			}
			if(items.length === darkAreas){
				console.log('Item con '+darkAreas+' zonas: ', JSON.stringify(pos))
				pos.isIlluminated = true;
				if(up) markAsLitUp(pos);
				if(right) markAsLitRight(pos);
				if(down) markAsLitDown(pos);
				if(left) markAsLitLeft(pos);
			}
		}
	})
}

/*
	Functions markAsLit (up, right, down and lect) recives an object
	{
		"row": 1,
		"column": 4,
		"value": "0",
		"isIlluminated": false
	}
*/
function markAsLitUp(pos) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == (pos.row-1) && element.column == pos.column && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitUp(item);
	}
}

function markAsLitRight(pos) {
	// console.log('markAsLitRight = ', JSON.stringify(pos))
	let item = _.find(processedMatrix, function(element) {
		return element.row == pos.row && element.column == (pos.column+1) && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitRight(item);
	}
}

function markAsLitDown(pos) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == (pos.row+1) && element.column == pos.column && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitDown(item);
	}
}

function markAsLitLeft(pos) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == pos.row && element.column == (pos.column-1) && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitLeft(item);
	}
}

console.log('Placing luminaires ...')
console.log('Evaluate matrix for 4 dark areas');
findDarkArea(4);
console.log('Finish process matrix for 4 dark areas');
// fs.writeFileSync('4DarkAreas.json', JSON.stringify(processedMatrix, null, 4), 'utf8')

console.log('Evaluate matrix for 3 dark areas');
findDarkArea(3);
// fs.writeFileSync('3DarkAreas.json', JSON.stringify(processedMatrix, null, 4), 'utf8')

console.log('Evaluate matrix for 2 dark areas');
findDarkArea(2);
// fs.writeFileSync('2DarkAreas.json', JSON.stringify(processedMatrix, null, 4), 'utf8')

console.log('Evaluate matrix for 1 dark areas');
findDarkArea(1);
// fs.writeFileSync('1DarkAreas.json', JSON.stringify(processedMatrix, null, 4), 'utf8')

console.log('Evaluate matrix for 0 dark areas');
findDarkArea(0);
// fs.writeFileSync('0DarkAreas.json', JSON.stringify(processedMatrix, null, 4), 'utf8')




















