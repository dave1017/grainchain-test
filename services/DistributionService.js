const debug = require('debug')('grainchain-test:services:DistributionService');
const utf8FileContent = require('../utils/files').readUtf8File;
const _ = require('lodash');

module.exports = {
	calculate: async (filePath) => {
		try {
			let fileData;
			let processedMatrix = [];
			let itemNumber = 0;

			fileData = await utf8FileContent(filePath)
			// Parse data
			debug('fileData ', fileData);
			fileData = fileData.split(/\n/)
			debug('fileData ', fileData);
			for(let i=0; i<fileData.length; i++){
				fileData[i] = fileData[i].replace(/,/g,'');
			}
			debug('fileData ', JSON.stringify(fileData, null, 4));

			for(let i=0;i<fileData.length;i++){
				for(let j=0;j<fileData[i].length;j++){
					processedMatrix.push({
						itemNumber: itemNumber,
						row: i,
						column: j,
						value: fileData[i][j],
						isIlluminated: false,
						isSetted: false
					});
					itemNumber++;
				}
			}

			// Processing data
			debug('Estimating luminaires ...')
			debug('Evaluate matrix for 4 dark areas');
			findDarkArea(4, processedMatrix);
			debug('Finish process matrix for 4 dark areas');

			debug('Evaluate matrix for 3 dark areas');
			findDarkArea(3, processedMatrix);

			debug('Evaluate matrix for 2 dark areas');
			findDarkArea(2, processedMatrix);

			debug('Evaluate matrix for 1 dark areas');
			findDarkArea(1, processedMatrix);

			debug('Evaluate matrix for 0 dark areas');
			findDarkArea(0, processedMatrix);


			return {
				matrixDimentions: {
					x: fileData[0].length,
					y: fileData.length
				},
				matrix: processedMatrix
			};
		} catch(error) {
			debug('Error: ', error)
			return {
				message: 'Error to process file ' + filePath,
				error: error
			}
		}
	}
}

function findDarkArea(darkAreas, processedMatrix){
	processedMatrix.forEach(function(pos) {
		if(!pos.isIlluminated && pos.value == 0){
			// debug('Process position')
			// debug(JSON.stringify(pos));
			let items = [];
			let item, up, right, down, left;

			let findDarkAreaUpVal = findDarkAreaUp(pos, processedMatrix);
			let findDarkAreaRightVal = findDarkAreaRight(pos, processedMatrix);
			let findDarkAreaDownVal = findDarkAreaDown(pos, processedMatrix);
			let findDarkAreaLeftVal = findDarkAreaLeft(pos, processedMatrix);

			if(findDarkAreaUpVal) {
				items.push(findDarkAreaUpVal);
				up = true;
			}

			if(findDarkAreaRightVal) {
				items.push(findDarkAreaRightVal);
				right = true;
			}

			if(findDarkAreaDownVal) {
				items.push(findDarkAreaDownVal);
				down = true;
			}

			if(findDarkAreaLeftVal) {
				items.push(findDarkAreaLeftVal);
				left = true;
			}

			// debug('ITEMS ', items)
			// debug('POSITION ', JSON.stringify(pos))
			if(items.length === darkAreas){
				debug('Item with '+darkAreas+' zone: ', JSON.stringify(pos))
				pos.isIlluminated = true;
				pos.isSetted = true;
				if(up) markAsLitUp(pos, processedMatrix);
				if(right) markAsLitRight(pos, processedMatrix);
				if(down) markAsLitDown(pos, processedMatrix);
				if(left) markAsLitLeft(pos, processedMatrix);
			}
		}
	})
}

function findDarkAreaUp(pos, processedMatrix){
	let columns = _.filter(processedMatrix, function(o) {
		return o.row < pos.row && o.column == pos.column;
	});
	columns = columns.reverse();
	let find = 0;
	// debug('findDarkAreaUp ', JSON.stringify(columns, null, 4))
	columns.every(column => {
		if(column.row < pos.row && column.value == 0 && !column.isIlluminated) {
			find++;
		} else if(column.value == 1) {
			return false;
		}
	});
	if(find == 0){
		return false;
	} else {
		return true;
	}
}

function findDarkAreaRight(pos, processedMatrix){
	// let rows = _.filter(processedMatrix, { 'row': pos.row });
	let rows = _.filter(processedMatrix, function(o) {
		return o.row == pos.row && o.column > pos.column;
	});
	let find = 0;
	// debug('findDarkAreaRight ', JSON.stringify(rows, null, 4))
	rows.every(row => {
		if(row.column > pos.column && row.value == 0 && !row.isIlluminated) {
			find++;
		} else if(row.value == 1) {
			return false;
		}
	})
	if(find == 0){
		return false;
	} else {
		return true;
	}
}

function findDarkAreaDown(pos, processedMatrix){
	// let columns = _.filter(processedMatrix, { 'column': pos.column });
	let columns = _.filter(processedMatrix, function(o) {
		return o.row > pos.row && o.column == pos.column;
	});
	let find = 0;
	// debug('findDarkAreaDown ', JSON.stringify(columns, null, 4))
	columns.every(column => {
		if(column.row > pos.row && column.value == 0 && !column.isIlluminated) {
			find++;
		} else if(column.value == 1) {
			return false;
		}
	})
	if(find == 0){
		return false;
	} else {
		return true;
	}
}

function findDarkAreaLeft(pos, processedMatrix){
	// let rows = _.filter(processedMatrix, { 'row': pos.row });
	let rows = _.filter(processedMatrix, function(o) {
		return o.row == pos.row && o.column < pos.column;
	});
	rows = rows.reverse();
	let find = 0;
	// debug('findDarkAreaLeft ', JSON.stringify(rows, null, 4))
	rows.every(row => {
		if(row.column < pos.column && row.value == 0 && !row.isIlluminated) {
			find++;
		} else if(row.value == 1) {
			return false;
		}
	})
	if(find == 0){
		return false;
	} else {
		return true;
	}
}

/*
	Functions markAsLit (up, right, down and lect) recives an object
	{
		"row": 1,
		"column": 4,
		"value": "0",
		"isIlluminated": false
	}
*/
function markAsLitUp(pos, processedMatrix) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == (pos.row-1) && element.column == pos.column && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitUp(item, processedMatrix);
	}
}

function markAsLitRight(pos, processedMatrix) {
	// debug('markAsLitRight = ', JSON.stringify(pos))
	let item = _.find(processedMatrix, function(element) {
		return element.row == pos.row && element.column == (pos.column+1) && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitRight(item, processedMatrix);
	}
}

function markAsLitDown(pos, processedMatrix) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == (pos.row+1) && element.column == pos.column && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitDown(item, processedMatrix);
	}
}

function markAsLitLeft(pos, processedMatrix) {
	let item = _.find(processedMatrix, function(element) {
		return element.row == pos.row && element.column == (pos.column-1) && element.value == 0;
	});
	if(item) {
		processedMatrix[item.itemNumber].isIlluminated = true;
		markAsLitLeft(item, processedMatrix);
	}
}