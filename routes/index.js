const debug = require('debug')('grainchain-test:routes:index');
const express = require('express');
const router = express.Router();
const multer  = require('multer');
const upload = multer({ dest: 'upload/'});
const distributionController = require('../controllers/distributionController');

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', {
		title: 'Grain Chain Test',
		layout: 'layout'
	});
});

router.post('/upload', [
		upload.single('file')
	],
	distributionController.estimate
);

module.exports = router;
