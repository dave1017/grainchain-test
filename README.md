# Solución a Prueba de lógica GrainChain

La solución al ejercicio de lógica se encuentra contenido en un aplicativo tipo server side, se penso así por la practicidad a la hora de hacer pruebas y por el soporte a la representación gráfica.

La solución esta codificada en javascript y se ejecuta utilizando NodeJS

## Tecnologías que se utilizaron para la solución

* NodeJS (JavaScript)
* HTML
* CSS
* Handlebars (Como motor de renderizado)

Plugins y librerias:

* ExpressJS
* Bootstrap
* JQuery
* Dropzone JS


Como ejecutar el proyecto:

## Prerrequisitos:

* Se debe tener instalado [NodeJS con NPM](https://nodejs.org/es/) en su última versión
* Se debe tener instalado [Git](https://git-scm.com/)

## Ejecución

1. Descargue este repositorio en su branch master en una ubicación que crea conveniente
2. Diríjase vía terminal de comandos a la carpeta del proyecto
3. Ejecute el siguiente comando: $ **npm install**, el cual instalará todos los módulos y dependencias que necesita el proyecto
4. Después ejecuté el siguiente comando: $ **npm run start**
5. Después abra su navegar web, de preferencia Chrome o Firefox y diríjase a la siguiente URL: **http://localhost:3000**
6. En el navegar se encontran las instrucciones para usar el aplicativo y realizar las pruebas correspondientes

## Pruebas

Dentro del proyecto en la carpeta **docs** se encuentra un archivo de ejemplo (example.txt) a cargar para realizar pruebas

El aplicativo espera recibir un archivo txt con el siguiente formato:


	0,0,0,1,0,1,1,1
	0,1,0,0,0,0,0,0
	0,1,0,0,0,1,1,1
	0,1,1,1,1,0,0,0
	0,1,0,0,1,0,0,0
	0,1,0,0,1,0,1,0
	0,1,0,1,1,0,1,0
	0,0,0,0,1,0,0,0
	0,1,1,0,1,1,0,1
	0,0,0,0,1,0,0,0
	1,0,0,1,0,0,1,1
	0,0,1,0,0,0,0,0


`

## Lógica de la solución

Toda la lógica de negocio esta contenida en el archivo **DistributionService.js** en la ruta **services/DistributionService.js**

