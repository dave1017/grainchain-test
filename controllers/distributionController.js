const debug = require('debug')('grainchain-test:controllers:distributionController');
const DistributionService = require('../services/DistributionService')

module.exports = {
	estimate: async (req, res, next) => {
		let filePath = req.file.path;
		try {
			let response = await DistributionService.calculate(filePath);
			res.json(response);
		} catch(error) {
			res.status(500).json(error)
		}
	}
}