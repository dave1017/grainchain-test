const fs = require('fs');
const util = require('util');

module.exports = {
	readUtf8File: async(filePath) => {
		const readFile = util.promisify(fs.readFile);
		return await readFile(filePath, 'utf8');
		// return  fs.readFile(filePath, 'utf8')
	}
}